# ics-ans-role-supercycles-reftabs

Ansible role to deploy supercycles Reference & Lookup Tables.

## Role Variables

```yaml
supercycles_reftabs_repo: https://gitlab.esss.lu.se/icshwi/reftabs.git
supercycles_reftabs_version: master
supercycles_reftabs_dest: /opt/reftabs
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-supercycles-reftabs
```

## License

BSD 2-clause
